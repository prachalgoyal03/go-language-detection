package main

import (
	_ "trell/go-language-detection/config"
	"trell/go-language-detection/server"
)

func main() {
	server.Init()
}
