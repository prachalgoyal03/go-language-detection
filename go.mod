module trell/go-language-detection

go 1.14

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/joho/godotenv v1.3.0
	github.com/olivere/elastic/v7 v7.0.14
	go.elastic.co/apm/module/apmelasticsearch v1.7.2
	go.elastic.co/apm/module/apmgin v1.7.2
	go.elastic.co/apm/module/apmgoredis v1.7.2
	go.elastic.co/apm/module/apmsql v1.7.2
	go.uber.org/zap v1.14.1
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2
)
