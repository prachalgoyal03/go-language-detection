package server

import (
	"trell/go-language-detection/db"
	// "trell/go-starter/es"
	"trell/go-language-detection/logger"
	//"trell/go-starter/redis"
)

func Init() {
	logger.Init()
	db.Init()
	// es.Init()
	//redis.Init()

	r := NewRouter()
	r.Run(":" + "4000")
}
